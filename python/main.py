import json
import socket
import sys

class FerrarAI(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def drive(self, data):
        for car in data:
            if(car['id']['name'] == 'Ferrar-AI'):
                pieceIndex = car['piecePosition']['pieceIndex']
                piece = self.pieces[pieceIndex]
                length = piece.get('length')
                if(length is None):
                    length = (piece['angle']/360) * (2*3.147*piece['radius'])
                inPieceDistance = car['piecePosition']['inPieceDistance']
                momentum = inPieceDistance - self.prev_pos
                if(momentum > 5):
                    vis = 1.8
                else:
                    vis = 1.2
                self.prev_pos = car['piecePosition']['inPieceDistance']
                #radius = self.piece.get('radius')
                if(abs(car['angle'])<self.drift):
                    self.speed = abs(abs(car['angle']) - self.drift)/self.drift
                else:
                    self.speed = 0.001
                if(pieceIndex in self.in_turn and car['angle'] > self.drift):
                    self.speed = 0.1
                if(pieceIndex in self.turn_ahead and inPieceDistance > length/vis):
                    self.speed = 0.35
                if momentum > 8: self.speed = 0.8
                #print self.speed, car['angle'], length, inPieceDistance, pieceIndex
            else:
                print car
        #tick = data['gameTick']
        #direction = "Right"
        #self.switch_lane(direction)
        self.throttle(self.speed)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.drive(data)

    def on_crash(self, data):
        print data['name'] + " crashed"
        #self.drift = 55.0
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def calculate(self, data):
        race = data['race']
        track = race['track']
        cars = race['cars']
        lanes = track['lanes']
        self.speed = 1.0
        self.drift = 1.0
        self.pieces = track['pieces']
        self.prev_pos = 0.0
        self.turn_ahead = []
        self.in_turn = []
        for i,piece in enumerate(self.pieces):
            angle = piece.get('angle') 
            if(angle is not None):
                self.in_turn.append(i)
                self.turn_ahead.append(i-1)
        track_name = track['name']
        print "In Track: " + track_name
        print "Total no.of Pieces in track: ", len(self.pieces)
        print "Total no.of Lanes in track: ", len(lanes)
        print "Total no.of Cars in track: ", len(cars)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.calculate,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if(sys.platform == "win32"):
        sys.argv = [sys.argv[0], 'hakkinen.helloworldopen.com', '8091', 'Ferrar-AI','Vzw2KJTknHMlTw']
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = FerrarAI(s, name, key)
        bot.run()
